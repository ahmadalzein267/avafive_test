import { DataSource, DataSourceOptions } from 'typeorm';
import { User } from '../entities/user.entity';

export const dataSourceOptions: DataSourceOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: '',
  database: 'avafive_user', // I kept the database name to know the my naming technique
  entities: [User],
  migrations: ['dist/apps/user/apps/user/src/db/migrations/*.js'],
};

const dataSource = new DataSource(dataSourceOptions);
export default dataSource;
