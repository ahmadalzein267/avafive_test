import { Body, Controller, Get, Post, Query, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginDto } from './dto/login.dto';
import { GetUsersDto } from './dto/get-users.dto';
import { Constants } from 'libs/common/constants';
import { Payload } from '@app/common/types/payload';
import { AuthGuard } from '@nestjs/passport';
import { AdminGuard } from './guards/admin.guard';

@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  async register(@Body() body: CreateUserDto) {
    const user = await this.userService.create(body);
    const payload: Payload = {
      id: user.id,
      password: user.password,
    };
    const token = await this.userService.signPayload(payload);
    return { success: true, data: { ...user, password: undefined }, token };
  }

  @Post('login')
  async login(@Body() body: LoginDto) {
    const user = await this.userService.getByLogin(body);
    const payload: Payload = {
      id: user.id,
      password: user.password,
    };
    const token = await this.userService.signPayload(payload);
    return { success: true, data: { ...user, password: undefined }, token };
  }

  @Get()
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  async getUsers(@Query() query: GetUsersDto) {
    const [users, pageSize, currentPage, totalPages, usersCount] =
      await this.userService.get(query);
    return {
      success: true,
      data: { users, pageSize, currentPage, totalPages, usersCount },
    };
  }
}
