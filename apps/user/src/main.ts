import { NestFactory } from '@nestjs/core';
import { UserModule } from './user.module';
import { config } from 'dotenv';
import { Transport } from '@nestjs/microservices';
import { ValidationPipe } from '@nestjs/common';
import { HttpExceptionFilter } from '@app/common/filters/http-exception.filter';

config({ path: `apps/user/.env` });

async function bootstrap() {
  const app = await NestFactory.create(UserModule);

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    }),
  );
  app.useGlobalFilters(new HttpExceptionFilter());

  await app.listen(4501);
}
bootstrap();
