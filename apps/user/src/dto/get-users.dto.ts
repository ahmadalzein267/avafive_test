import { PaginationQueryDto } from '@app/common/dto/pagination-query.dto';
import { IsOptional, IsString } from 'class-validator';

export class GetUsersDto extends PaginationQueryDto {
  @IsString()
  @IsOptional()
  email?: string;
}
