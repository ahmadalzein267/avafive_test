import { IsEmail, IsString, IsStrongPassword, Matches } from 'class-validator';

export class CreateUserDto {
  @IsEmail()
  email: string;

  @IsStrongPassword()
  password: string;
}
