import { Payload } from '@app/common/types/payload';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { sign } from 'jsonwebtoken';
import { User } from './entities/user.entity';
import { ILike, Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginDto } from './dto/login.dto';
import { GetUsersDto } from './dto/get-users.dto';
import { Constants } from 'libs/common/constants';
import { compare } from 'bcrypt';
import { verify } from 'jsonwebtoken';
import { SharedService } from '@app/common/services/shared.service';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly sharedService: SharedService,
  ) {}

  async signPayload(payload: Payload) {
    return sign(payload, process.env.SECRET_KEY, {
      expiresIn: process.env.JWT_EXPIRY,
    });
  }

  async create(input: CreateUserDto) {
    await this.checkUniqueEmail(input.email);
    const user = await this.userRepository.create({
      ...input,
      status: Constants.USER,
      password: await this.sharedService.hashString(input.password),
    });
    await this.userRepository.save(user);
    return await this.getByIdWithPassword(user.id);
  }

  async get(input: GetUsersDto) {
    const usersCount = await this.userRepository.count({
      where: {
        email: ILike(`%${input.email ?? ''}%`),
      },
    });
    const users = await this.userRepository.find({
      where: {
        email: ILike(`%${input.email ?? ''}%`),
      },
      skip: input.limit * (input.page - 1),
      take: input.limit,
    });

    return [
      users,
      input.limit,
      input.page,
      Math.ceil(usersCount / input.limit),
      usersCount,
    ];
  }

  async getById(id: string) {
    const user = await this.userRepository.findOne({
      where: {
        id,
      },
    });

    if (!user) throw new HttpException('User not found', HttpStatus.NOT_FOUND);

    return user;
  }

  async getByIdWithPassword(id: string) {
    const user = await this.userRepository.findOne({
      where: {
        id,
      },
      select: {
        id: true,
        password: true,
        email: true,
        status: true,
      },
    });

    if (!user) throw new HttpException('User not found', HttpStatus.NOT_FOUND);

    return user;
  }

  async getByLogin(input: LoginDto) {
    const user = await this.userRepository.findOne({
      where: {
        email: input.email,
      },
      select: {
        id: true,
        email: true,
        password: true,
        status: true,
      },
    });

    if (!user)
      throw new HttpException('Invalid Credentials', HttpStatus.UNAUTHORIZED);

    if (!(await compare(input.password, user.password)))
      throw new HttpException('Invalid Credentials', HttpStatus.UNAUTHORIZED);

    return user;
  }

  async checkUniqueEmail(email: string) {
    const user = await this.userRepository.findOne({
      where: {
        email,
      },
    });

    if (user)
      throw new HttpException('Email is already used', HttpStatus.BAD_REQUEST);
  }
}
