/* eslint-disable prettier/prettier */
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy, VerifiedCallback } from 'passport-jwt';

import { UserService } from './user.service';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  private client: ClientProxy;
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.SECRET_KEY,
    });

    this.client = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        port: 4555,
      },
    });
  }

  async validate(payload: any, done: VerifiedCallback) {
    const user = await this.client
      .send(
        { cmd: 'VALIDATE_USER' },
        {
          ...payload,
        },
      )
      .toPromise();

    if (!user) {
      return done(
        new HttpException('Not Authorized', HttpStatus.UNAUTHORIZED),
        false,
      );
    }

    return done(null, user, payload.iat);
  }
}
