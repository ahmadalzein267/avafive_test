import {
  IsEmail,
  IsJWT,
  IsString,
  IsStrongPassword,
  IsUUID,
  Matches,
} from 'class-validator';

export class ValidateUserDto {
  @IsJWT()
  jwt: string;
}
