import { NestFactory } from '@nestjs/core';
import { AuthModule } from './auth.module';
import { ValidationPipe } from '@nestjs/common';
import { HttpExceptionFilter } from '@app/common/filters/http-exception.filter';
import { Transport } from '@nestjs/microservices';
import { config } from 'dotenv';

config({ path: `apps/auth/.env` });

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AuthModule, {
    options: { port: 4555 },
    host: 'localhost',
    transport: Transport.TCP,
  });

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    }),
  );
  app.useGlobalFilters(new HttpExceptionFilter());

  await app.listen();
}
bootstrap();
