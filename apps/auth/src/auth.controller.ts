import { Body, Controller, Get, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { MessagePattern } from '@nestjs/microservices';
import { ValidateUserDto } from './dto/validate-user.dto';
import { Payload } from '@app/common/types/payload';

@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @MessagePattern({ cmd: 'VALIDATE_USER' })
  async validateUser(@Body() payload: Payload) {
    console.log(payload);
    return await this.authService.validate(payload);
  }
}
