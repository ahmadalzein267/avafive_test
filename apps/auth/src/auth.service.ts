import { Payload } from '@app/common/types/payload';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'apps/user/src/entities/user.entity';
import { Repository } from 'typeorm';
import { verify } from 'jsonwebtoken';
[];

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}
  async validate(payload: Payload) {
    return await this.userRepository.findOne({
      where: {
        id: payload.id,
        password: payload.password,
      },
      select: {
        id: true,
        email: true,
        status: true,
      },
    });
  }
}
