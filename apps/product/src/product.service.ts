import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { ILike, Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { GetProductsDto } from './dto/get-products.dto';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
  ) {}
  async create(userId: string, input: CreateProductDto) {
    const product = await this.productRepository.create({
      ...input,
      userId,
    });
    await this.productRepository.save(product);
    return product;
  }

  async get(userId: string, input: GetProductsDto) {
    const productsCount = await this.productRepository.count({
      where: {
        userId,
        name: ILike(`%${input.name ?? ''}%`),
      },
    });
    const products = await this.productRepository.find({
      where: {
        userId,
        name: ILike(`%${input.name ?? ''}%`),
      },
      order: {
        price: 'DESC',
      },
      skip: input.limit * (input.page - 1),
      take: input.limit,
    });

    return [
      products,
      input.limit,
      input.page,
      Math.ceil(productsCount / input.limit),
      productsCount,
    ];
  }
}
