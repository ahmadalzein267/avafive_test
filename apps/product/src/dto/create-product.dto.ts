import {
  IsEmail,
  IsNumber,
  IsPositive,
  IsString,
  IsStrongPassword,
  Matches,
} from 'class-validator';

export class CreateProductDto {
  @IsString()
  name: string;

  @IsNumber()
  @IsPositive()
  price: number;
}
