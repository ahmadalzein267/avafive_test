import { PaginationQueryDto } from '@app/common/dto/pagination-query.dto';
import { IsOptional, IsString } from 'class-validator';

export class GetProductsDto extends PaginationQueryDto {
  @IsString()
  @IsOptional()
  name?: string;
}
