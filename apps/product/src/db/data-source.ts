import { DataSource, DataSourceOptions } from 'typeorm';
import { Product } from '../entities/product.entity';

export const dataSourceOptions: DataSourceOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: '',
  database: 'avafive_product', // I kept the database name to know the my naming technique
  entities: [Product],
  migrations: ['dist/apps/product/apps/product/src/db/migrations/*.js'],
};

const dataSource = new DataSource(dataSourceOptions);
export default dataSource;
