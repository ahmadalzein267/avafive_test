import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { GetProductsDto } from './dto/get-products.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller()
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  async addProduct(@Request() req: any, @Body() body: CreateProductDto) {
    const product = await this.productService.create(req.user.id, body);
    return { success: true, data: product };
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async getProducts(@Request() req: any, @Query() query: GetProductsDto) {
    const [products, pageSize, currentPage, totalPages, productsCount] =
      await this.productService.get(req.user.id, query);
    return {
      success: true,
      data: { products, pageSize, currentPage, totalPages, productsCount },
    };
  }
}
