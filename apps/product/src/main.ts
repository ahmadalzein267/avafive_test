import { NestFactory } from '@nestjs/core';
import { ProductModule } from './product.module';
import * as dotenv from 'dotenv';
import { ValidationPipe } from '@nestjs/common';
import { HttpExceptionFilter } from '@app/common/filters/http-exception.filter';

dotenv.config({ path: `apps/product/.env` });

async function bootstrap() {
  const app = await NestFactory.create(ProductModule);

  app.enableCors();
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    }),
  );
  app.useGlobalFilters(new HttpExceptionFilter());

  await app.listen(+process.env.PORT);
}
bootstrap();
