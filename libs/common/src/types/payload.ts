/* eslint-disable prettier/prettier */
export interface Payload {
  id: string;
  password: string;
  iat?: number;
  expiresIn?: string;
}
