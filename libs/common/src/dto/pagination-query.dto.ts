import {
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
  Min,
} from 'class-validator';

export class PaginationQueryDto {
  @IsNumber()
  @IsPositive()
  @IsOptional()
  limit?: number = 10;

  @IsNumber()
  @IsPositive()
  @IsOptional()
  page?: number = 1;

  @IsString()
  @IsOptional()
  sortBy: string;
}
