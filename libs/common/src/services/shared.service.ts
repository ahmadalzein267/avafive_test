import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

@Injectable()
export class SharedService {
  async hashString(str: string) {
    const salt = await bcrypt.genSalt(+process.env.SALT);
    return await bcrypt.hash(str, salt);
  }
}
